
from mongo import MongoConnect

mongo_connect = MongoConnect()
db = mongo_connect.db
# Step 2: Create sample data
types = {'soups': '🍲 Супы', 'meat': '🍖 Мясo', 'side_dish': '🍝 Гарнир', 'meal': '🍝 Горячее', 'salad': '🥗 Салат'}

for key, value in types.items():
    meal_types = {
        'meal_index': key,
        'meal_name': value

    }
    # Step 3: Insert business object directly into MongoDB via insert_one
    result = db.meal_types.insert_one(meal_types)
