import logging
import os

from aiogram import Bot, Dispatcher, executor, types
from aiogram.utils.callback_data import CallbackData
from app.config import load_config
from datetime import datetime
from bson.objectid import ObjectId


from mongo import MongoConnect

today = datetime.today()
today_datetime = datetime(today.year, today.month, today.day)

current_dir = os.getcwd()

config = load_config("config/bot.ini")
bot = Bot(token=config.bot.TOKEN)

mongo_connect = MongoConnect()

dp = Dispatcher(bot)
# Включаем логирование, чтобы не пропустить важные сообщения
logging.basicConfig(level=logging.INFO)
callback_numbers = CallbackData("fabnum", "action", "id")


@dp.message_handler(commands="start")
async def cmd_random(message: types.Message):
    keyboard = types.InlineKeyboardMarkup(row_width=1)
    keyboard.add(types.InlineKeyboardButton(text="🍱 Меню", callback_data=callback_numbers.new(action="menu", id=1)))
    await message.answer("Кликните на меню", reply_markup=keyboard)


@dp.callback_query_handler(callback_numbers.filter(action=["menu"]))
async def send_random_value(call: types.CallbackQuery, callback_data: dict):
    action = callback_data["action"]
    keyboard = types.InlineKeyboardMarkup(row_width=1)
    results = mongo_connect.db.meal_types.find()

    meal_types = {x['meal_index']: x['meal_name'] for x in results}

    if action == "menu":
        for key, value in meal_types.items():
            keyboard.add(
                types.InlineKeyboardButton(
                    text=f"{value}",
                    callback_data=callback_numbers.new(action="meal_type", id=f"{key}"),
                )
            )
    await call.message.answer("Тапните на раздел", reply_markup=keyboard)


@dp.callback_query_handler(callback_numbers.filter(action=["meal_type"]))
async def send_random_value(call: types.CallbackQuery, callback_data: dict):
    meal_type = callback_data["id"]
    keyboard = types.InlineKeyboardMarkup(row_width=1)
    food = mongo_connect.db.menu.find({"09_date": today_datetime, "03_meal_type": meal_type})
    for _ in food:
        vegan = "🥬" if _['04_is_vegan'] else ""
        keyboard.add(
            types.InlineKeyboardButton(
                text=f"{_['01_meal_name']}{vegan}",
                callback_data=callback_numbers.new(action=f"{meal_type}", id=f"{_['_id']}")
            )
        )
    keyboard.add(types.InlineKeyboardButton(text="🍱 Меню", callback_data=callback_numbers.new(action="menu", id=1)))
    await call.message.answer(text="Тапните на блюдо", reply_markup=keyboard)


@dp.callback_query_handler(callback_numbers.filter(action=["soup", "side_dish", "salad", "meat", "meal"]))
async def send_random_value(call: types.CallbackQuery, callback_data: dict):
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    meal_type = callback_data["action"]
    _id = callback_data["id"]
    food = mongo_connect.db.menu.find_one({"09_date": today_datetime, "03_meal_type": meal_type, "_id": ObjectId(_id)})

    keyboard.add(types.InlineKeyboardButton(text="🍱 Меню", callback_data=callback_numbers.new(action="menu", id=1)))
    await call.message.answer(
        text=f"👩‍🍳 {food['01_meal_name']} \n📝 Состав: {food['02_recepie']} \n🍩 KБЖУ {food['05_kkal']} 🥚{food['06_proteins']} 🫒{food['07_fats']} 🥐{food['08_carbs']}",
        reply_markup=keyboard,
    )


if __name__ == "__main__":
    # Запуск бота
    executor.start_polling(dp, skip_updates=True)
