FROM quay.io/pypa/manylinux2014_x86_64
ENV SYS_SRC_DIR "${SYS_SRC_DIR:-/usr/local/src}"
ENV APP_DIR ${SYS_SRC_DIR}/arrival_bakery
ENV PYPI_INDEX=https://pypi.python.org/simple
RUN mkdir ${APP_DIR}
COPY ./ ${APP_DIR}
RUN /opt/python/cp38-cp38/bin/pip install --disable-pip-version-check --no-cache-dir -i ${PYPI_INDEX} -r ${APP_DIR}/requirements.txt
WORKDIR  "/usr/local/src/arrival_bakery"
CMD "/opt/python/cp38-cp38/bin/python main.py"
